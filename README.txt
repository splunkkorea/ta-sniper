Splunk add-on for WINS Sniper IDS.

Assumption
----------
1. Sniper logs will be delivered as syslog to Splunk using udp 514 port.
2. Syslog headers might vary depending on the settings on Sniper IDS but [ character is not included in the header part.

Installation
------------
# cd $SPLUNK_HOME/etc/apps/
# git clone -b master https://asohahn@bitbucket.org/splunkkorea/ta-sniper.git
# mv ta-sniper TA-sniper

Input Settings
--------------
Input settings can be configured either at Heavy Forwarder or Indexer directly modifing inputs.conf according to how it's guided in original Splunk inputs.conf.spec. But be aware that sourcetype SHOULD be literal 'sniper' without quotes.

inputs.conf example

[udp://514]
connection_host = ip
host = sniper_ids_1
sourcetype = sniper
index = main



